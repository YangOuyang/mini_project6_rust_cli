# mini_project8_rust_CLI

 Rust Command-Line Tool with Testing

 Requirements
Rust command-line tool
Data ingestion/processing
Unit tests

## Getting Started
1. Init the project with cargo
```bash
cargo init
```
2. Add dependencies to Cargo.toml
```toml
[dependencies]
clap = { version = "4.0", features = ["derive"] }
```
3. Add the CLI logic to main.rs which could find the pattern in the file

## Split the logic into functions


1. Put the find_matches function into a new src/lib.rs.
    1. Add a pub in front of the fn (so it’s pub fn find_matches) to make it something that users of our library can access.
    ```rust
    pub fn find_matches(content: &str, pattern: &str) {
        for line in content.lines() {
            if line.contains(pattern) {
                println!("{}", line);
            }
        }
    }
    ```
2. Remove find_matches from src/main.rs.
    In the fn main, prepend the call to find_matches with ::, so it’s now ::find_matches which means it uses the function from the library we just created.
    ```rust
    use mini_project8_rust_cli::find_matches;
    ```

## Tests
1. Add a new file in the tests directory called cli.rs.
2. Add the following code to tests/lib.rs
    ```rust
    #[test]
    fn find_a_match() {
        let content = "This is a test\nof the emergency broadcast system";
        let pattern = "test";
        find_matches(content, pattern);
    }
    ```
3. Run the tests
```bash
cargo test
```
4. Test success! ![alt text](image.png)