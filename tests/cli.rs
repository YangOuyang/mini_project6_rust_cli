use assert_cmd::prelude::*; // Add methods on commands
use assert_fs::prelude::*;
use mini_project8_rust_cli::find_matches;
use predicates::prelude::*; // Used for writing assertions
use std::process::Command; // Run programs

#[test]
fn find_a_match() {
    let content = "This is a test\nof the emergency broadcast system";
    let pattern = "test";
    find_matches(content, pattern);
    print!("This is a test\n");
}

#[test]
fn find_content_in_file() -> Result<(), Box<dyn std::error::Error>> {
    let file = assert_fs::NamedTempFile::new("sample.txt")?;
    file.write_str("A test\nActual content\nMore content\nAnother test")?;

    let mut cmd = Command::cargo_bin("mini_project8_rust_cli")?;
    cmd.arg("test").arg(file.path());
    cmd.assert()
        .success()
        .stdout(predicate::str::contains("A test\nAnother test"));

    Ok(())
}

#[test]
fn file_doesnt_exist() -> Result<(), Box<dyn std::error::Error>> {
    let mut cmd = Command::cargo_bin("mini_project8_rust_cli")?;

    cmd.arg("foobar").arg("test/file/doesnt/exist");
    cmd.assert()
        .failure()
        .stderr(predicate::str::contains("could not read file"));

    Ok(())
}
